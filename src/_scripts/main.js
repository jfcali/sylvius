// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';
var $ = require('jquery');
var io = require('socket.io-client')

/******************
* TIMEOUT HANDLER *
*******************/

var timeoutHandler = (function() {
	var timer,
		time,
		onEnd;

	var init = (config) => {
		time = config.time || 10000;
		onEnd = config.onEnd;
		setTimer();
	}

	var setTimer = () => {
		timer = setTimeout(()=> {
			onEnd();
		}, time);
	}

	var clear = () => {
		clearTimeout(timer);
	}

	var resetTimer = () => {
		clear();
		setTimer();
	}

	return {
		init: init,
		setTimer: setTimer,
		clear: clear,
		resetTimer: resetTimer
	}

})();

/*
*LANGUAGE LOADER MODULE
*/
var languageLoader = (function() {
	/*
	* language setup
	*/
	var languages = {};

	var load = function(totem) {
		return $.when(
			$.getJSON('../tmp/locale/ca.json', function(data) {
				languages.ca = (data);
				console.log('ca loaded')	
			}),

			$.getJSON('../tmp/locale/en.json', function(data) {
				languages.en = (data);
				console.log('en loaded')
			}),

			$.getJSON('../tmp/locale/fr.json', function(data) {
				languages.fr = (data);
				console.log('fr loaded')	
			}),

			$.getJSON('../tmp/locale/es.json').then(function(data) {
				languages.es = (data);
				console.log('es loaded')
			})
		);
	};
	
	var getLanguages = function() {
		return languages;
	};

	/*
	* setLocale for text
	Based on user and languages for each totem, searches all elements with data-label and replaces content. 
	Data-label should be equal to local key. ej: 
		->html 		
	  		<p data-label="text_n"> 
	  	->json
	  		{
				"text_n": "This text would be appended to p with data-label === text_n"
	  		}
	*/
   	var setLocale = function(locale, languages) {
    	var language = languages[locale];

    	var labels = $('[data-label]');

    	labels.map(function(l, e) {
    		var label = language[e.dataset.label];
    		$(e).html(label);
    	});
	};

	return {
		load: load,
		getLanguages: getLanguages,
		setLocale: setLocale
	};
})();


/*
*MAIN MODULE
*/
$(function() {

	var NAMESPACE = 'SYLVIUS';
	var user = {};
	var locale = 'ca';
	var sources = [
		'videos/placeholder/paciente.mp4',
		'videos/placeholder/cirugia.mp4',
		'videos/placeholder/lenguaje.mp4'
	];
	var userName;
	var current;

	window[NAMESPACE] = function() { 
		var $mainTitle = document.querySelector('[data-title]');
		var $menu = document.querySelector('[data-menu]');
		var $menuItems = Array.from(document.querySelectorAll('[data-item]'));
		var $loaders = Array.from(document.querySelectorAll('[data-loader]'));
		var $player = document.querySelector('[data-player]');
		var $close = Array.from(document.querySelectorAll('[data-close]'));
		var $closeSession = document.querySelector('[data-sessionclose]');
		var $intro = document.querySelector('[data-intro]');

		var video = document.querySelector('[data-main]');
		var $videos = Array.from(document.querySelectorAll('[data-player]'))

		$videos.map(function(v) {
			let vid = v;
			v.querySelector('video').addEventListener('ended', function() {
				console.log('ended');
				current.querySelector('video').pause();
				current.querySelector('video').currentTime = 0;
				$(current).removeClass('reveal')
				console.log('reset timer');
				timeoutHandler.resetTimer();
				$menuItems.map(function(m, i) {
					m.addEventListener('click', selectVideo);
					setTimeout(function() {
						console.log(i*200);
						m.classList.add('reveal');
						$(m).find('.loader').removeClass('reveal');
					}, i*200);
				});


			})
		})
		var languages;
		var sessionStarted = false;

		var socket = io.connect('http://localhost:4000');

		/*------------------ socket definitions ------------------*/
		socket.on('localSessionStart', function(data) {
			if(!sessionStarted) {
				locale = 'ca';
				if(data.user) {
					locale = data.user.locale !== 'csc' ? data.user.locale : (data.user.locale_alternative ? data.user.locale_alternative : 'ca');
					userName = data.user.name;
				}
				userAction();
			}
		});
		/*------------------ socket definitions ------------------*/

		$close.map(function(c) {
			c.addEventListener('click', function() {
				$menuItems.map(function(m, i) {
					m.addEventListener('click', selectVideo);
					setTimeout(function() {
						console.log(i*200);
						m.classList.add('reveal');

						$(m).find('.loader').removeClass('reveal');
					}, i*200);
				});
				$(current).removeClass('reveal');
				current.querySelector('video').pause();
				current.querySelector('video').currentTime = 0;

				console.log('reset timer');
				timeoutHandler.resetTimer();

			});
		});

		$closeSession.addEventListener('click', ()=>{
			location.href = location.href;
		});

		video.addEventListener('ended', function(){
			console.log('ended');

			console.log('reset timer');
			timeoutHandler.resetTimer();
			$menuItems.map(function(m, i) {
				m.addEventListener('click', selectVideo);
				setTimeout(function() {
					console.log(i*200);
					m.classList.add('reveal');
					$(m).find('.loader').removeClass('reveal');
				}, i*200);
			});
			$player.classList.remove('reveal');
		});

		/*video.addEventListener('click', ()=> {
			console.log(video.duration)
			video.currentTime = video.duration - 1;
		});*/

		var loadVideo = function() {
			var req = new XMLHttpRequest();
			req.open('GET', 'videos/test.mp4', true);
			req.responseType = 'blob';

			req.onload = function() {
			   // Onload is triggered even on 404
			   // so we need to check the status code
			   console.log('loaded');
			   if (this.status === 200) {
			      var videoBlob = this.response;
			      var vid = URL.createObjectURL(videoBlob); // IE10+
			      var video = document.querySelector('[data-vid]');
			      console.log(video, vid, videoBlob);
			      // Video is now downloaded
			      // and we can set it as source on the video element
				  let src = video.querySelector('source').src;
				  src = vid;
			 	  video.querySelector('source').src = src;
			 	  video.play();
				  $player.classList.add('reveal');

			   }
			}
			req.onerror = function() {
			   // Error
			   console.log('errer');
			}

			req.send();
		}

		var loadVideoLocal = function(index) {
			/*ideo.autoPlay = true;
		  	$player.classList.add('reveal');
			var source = video.querySelector('source');
			source.src = sources[index];
			video.load();*/
			setTimeout(function () {      
			  // Resume play if the element if is paused.
			  /*if (video.paused) {
			    video.play();
			  }*/
			}, 50);
			//$(video).append(source);
			//$('.player').append(video);
			current = $videos[index];
			$(current).addClass('reveal');
			console.log(current.querySelector('video'));
			current.querySelector('video').play();
		}

		var loadUrls = function(locale) {
			languageLoader.load().then(function(data) {
				languages = languageLoader.getLanguages();
				languageLoader.setLocale(locale, languages);
				console.log('langs', languages, data);
			});
		};

		var selectVideo = function(e) {
			var which = e.target.dataset.item;
			loadVideoLocal(which);

			console.log('clear timer');
			timeoutHandler.clear();
			$menuItems.map(function(m) {
				m.removeEventListener('click', selectVideo);
				if(m.dataset.item === which) {
					m.classList.add('selected');
					$(m).find('.loader').addClass('reveal');
				} else {
					m.classList.remove('reveal');
				}
			});
			console.log(e.target.classList.add('this-one'));
		};

		var revealMenuItems = function(e) {
			console.log('revealing menu');

			console.log('init timer');
			timeoutHandler.init({
				time: 10000,
				onEnd: function() {
					location.href = location.href;
				}
			})

			$menu.removeEventListener('transitionend', revealMenuItems);
			$menuItems.map(function(m, i) {
				m.addEventListener('click', selectVideo);
				setTimeout(function() {
					console.log(i*200);
					m.classList.add('reveal');
				}, i*200);
			});
		};

		var revealMenu = function(e) {
			$menu.classList.add('reveal');
			$menu.addEventListener('transitionend', revealMenuItems);
		};

		var mainTitleAnimationEnd = function(e) {
			setTimeout(revealMenu, 2000);
		};

		var userAction = function() {
			$('.screensaver').remove();
			sessionStarted = true;
			loadUrls(locale);
			var src = $($intro).attr('src');
			src = src.replace('placeholder', locale);
			console.log(src);
			$videos.map(function(v) {
				let source = v.querySelector('source');
				console.log(source);
				source.src = source.src.replace('placeholder', locale);
				v.querySelector('video').load();
			})
			for(let i = 0; i < 3; i++ ) {
				sources[i] = sources[i].replace('placeholder', locale);
			}
			console.log('sources', sources)
;
			$($intro).attr('src', src);
			let greeting = '';
			if(userName) {
				switch(locale) {
					case 'ca': 
					greeting = 'Hola, ';
					break;
					case 'es': 
					greeting = 'Hola, ';
					break;
					case 'en': 
					greeting = 'Hello, ';
					break;
					case 'fr': 
					greeting = 'Bonjour, ';
					break;
					case 'csc': 
					greeting = 'Hola, ';
					break;
				}
				document.querySelector('[data-user]').textContent = greeting + userName;
			}
			setTimeout(function() {
				$mainTitle.classList.add('reveal');
				$mainTitle.addEventListener('transitionend', mainTitleAnimationEnd);
			}, 500);
		};

		var init = function() {
			//loadUrls('ca');
			console.log('init');
			$('.screensaver').click(function(e) {
				userAction();
			});



		};

		init();
	}; 

	var main = new window[NAMESPACE]();


});


